import { Component } from '@angular/core';
import {ActivatedRoute, Event, NavigationEnd, NavigationError, NavigationStart, Router} from "@angular/router";
import {Location} from "@angular/common";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'app';
  isSidebarMinified:boolean = false;
  isSetup: boolean = true;
  isDashboardActive:boolean = false;
  isPlanActive:boolean = false;


  constructor( private location: Location,
               private router: Router)
  {
      router.events.subscribe((event: Event) => {
        if (event instanceof NavigationEnd) {
          this.isSetup = location.isCurrentPathEqualTo('/care-plan/setup');
          this.isSidebarMinified = !location.isCurrentPathEqualTo('/care-plan/setup');
          this.isDashboardActive = this.doesCurrentPathContain(location.path(), 'dashboard');
          this.isPlanActive = (!this.isDashboardActive)? this.doesCurrentPathContain(location.path(), 'care-plan') : false;
        }

        if (event instanceof NavigationError) {
          // Hide loading indicator
          // Present error to user
          console.log(event.error);
        }
      });
  }

  private doesCurrentPathContain(path,contains){
    let pathArr = path.split('/'),
      count = 0;
    for(let sector of pathArr){
      if(sector === contains)
        count++;
    }

    return (count>0);
  }

  toggleSidebar(){
    this.isSidebarMinified = !this.isSidebarMinified;
  }
}
