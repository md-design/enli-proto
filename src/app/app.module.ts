import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import {HxUiModule} from "@hxui/angular";
import {PageNotFoundComponent} from "./page-not-found/page-not-found.component";
import {SharedModule} from "./shared/shared.module";
import {SetupModule} from "./care-plan/setup/setup.module";
import {CarePlanModule} from "./care-plan/care-plan.module";
import {StoreModule} from "@ngrx/store";
import {taskReducer} from "./care-plan/plan/reducers/task.reducer";
import {EffectsModule} from "@ngrx/effects";
import {StoreDevtoolsModule} from "@ngrx/store-devtools";
import {TaskEffects} from "./care-plan/plan/effects/task.effects";
import {DashboardModule} from "./care-plan/dashboard/dashboard.module";
import {TrackingModule} from "./tracking/tracking.module";


@NgModule({
  declarations: [
    AppComponent,
    PageNotFoundComponent
  ],
  imports: [
    BrowserModule,
    SharedModule.forRoot(),
    SetupModule,
    CarePlanModule,
    DashboardModule,
    TrackingModule,
    StoreModule.forRoot( {tasks: taskReducer}),
    EffectsModule.forRoot([TaskEffects]),
    StoreDevtoolsModule.instrument({maxAge:5}),
    HxUiModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
