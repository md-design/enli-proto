import { Component, OnInit } from '@angular/core';
import {Goal} from "../../shared/components/goals/goal.model";
import {ITeamMembers, SharingService} from "../sharing/sharing.service";
import {Observable} from "rxjs/Observable";

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  primaryGp: Goal = new Goal({title:"Primary GP", description: 'Dr Bob Loblaw'});
  patientGoal: Goal = new Goal({title:"Haoratio's goal", description: '“I’d like to be able to walk to the end of the street without becoming short of breath.”'});

  teamMembers$: Observable<ITeamMembers[]>;
  constructor(private service: SharingService) {
    this.teamMembers$ = this.service.getTeamMembers();
  }

  ngOnInit() {
  }

  hasAssigned(member:ITeamMembers){
    return this.service.hasAssigned(member);
  }

  getProviderTypeName(type){
    return this.service.getProviderTypeName(type);
  }

}
