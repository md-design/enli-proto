import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PlanComponent } from './plan.component';
import {SharedModule} from "../../shared/shared.module";
import { GoalsComponent } from '../../shared/components/goals/goals.component';
import { MetricsMeasurementsComponent } from './metrics-measurements/metrics-measurements.component';
import { TasksCorrespondenceComponent } from './tasks-correspondence/tasks-correspondence.component';
import { PatientTimelineComponent } from './patient-timeline/patient-timeline.component';
import { ArtefactTypePipe } from './patient-timeline/artefact-type.pipe';
import { PatientTimelineDetailComponent } from './patient-timeline-detail/patient-timeline-detail.component';
import { PlanService } from "./plan.service";
import { SummaryComponent } from './patient-timeline-detail/summary/summary.component';
import { TaskModalComponent } from "./tasks-correspondence/tasks-modal.component";
import {PeopleByTypePipe} from "./patient-timeline-detail/people-by-type.pipe";

@NgModule({
  imports: [
    CommonModule,
    SharedModule
  ],
  declarations: [
    PlanComponent,
    MetricsMeasurementsComponent,
    TasksCorrespondenceComponent,
    PatientTimelineComponent,
    ArtefactTypePipe,
    PeopleByTypePipe,
    PatientTimelineDetailComponent,
    SummaryComponent,
    TaskModalComponent
  ],
  entryComponents: [TaskModalComponent],
  providers:[
    PlanService
  ]
})
export class PlanModule { }
