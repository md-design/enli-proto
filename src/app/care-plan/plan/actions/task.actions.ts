import {TaskModel} from "../models/task.model";

export const LOAD_TASKS = 'LOAD_TASKS';
export const LOAD_TASKS_SUCCESS = 'LOAD_TASKS_SUCCESS';
export const LOAD_TASKS_ERROR = 'LOAD_TASKS_ERROR';

export const ADD_TASK = 'ADD_TASK';
export const ADD_TASK_SUCCESS = 'ADD_TASK_SUCCESS';


export class LoadTasksAction {
  readonly type = LOAD_TASKS;
  constructor(){}
}

export class LoadTasksSuccessAction {
  readonly type = LOAD_TASKS_SUCCESS;
  constructor(public payload: TaskModel[]){}
}

export class LoadTasksErrorAction {
  readonly type = LOAD_TASKS_ERROR;
  constructor(public payload: TaskModel[]){}
}

export class AddTaskAction {
  readonly type = ADD_TASK;
  constructor(public payload: TaskModel){}
}

export class AddTaskSuccessAction {
  readonly type = ADD_TASK_SUCCESS;
  constructor(public payload: TaskModel){}
}


export type Action
 = LoadTasksAction
 | LoadTasksSuccessAction
 | LoadTasksErrorAction
 | AddTaskAction
 | AddTaskSuccessAction

