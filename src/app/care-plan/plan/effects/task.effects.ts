import {Injectable} from "@angular/core";
import {PlanService} from "../plan.service";
import {Effect, Actions} from "@ngrx/effects";
import * as taskActions from './../actions/task.actions';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/map';


@Injectable()
export class TaskEffects {
  constructor(
    private planService: PlanService,
    private actions$: Actions
  ){}

  @Effect() loadTasks$ = this.actions$
    .ofType(taskActions.LOAD_TASKS)
    .switchMap(()=> this.planService.loadTasks())
    .map(tasks=> (new taskActions.LoadTasksSuccessAction((tasks))));

  @Effect() addTask$ = this.actions$
    .ofType(taskActions.ADD_TASK)
    .switchMap(action => this.planService.addTask((action as any).payload))
    .map(task => (new taskActions.AddTaskSuccessAction(task)));

}
