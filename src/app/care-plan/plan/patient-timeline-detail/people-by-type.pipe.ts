import { Pipe, PipeTransform } from '@angular/core';
import {PeopleModel} from "../models/people.model";
import {ProviderType, TaskModel} from "../models/task.model";

@Pipe({name: 'peopleByType'})
export class PeopleByTypePipe implements PipeTransform {
  transform(tasks: TaskModel[], linkedMember: PeopleModel): TaskModel[] {
    if(typeof linkedMember!==undefined)
        console.log(tasks.filter(task=> task.providers[0] === linkedMember.type));
    return (typeof linkedMember!==undefined)? tasks.filter(task=> task.providers[0] === linkedMember.type) : tasks;
  }
}
