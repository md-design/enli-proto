import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PatientTimelineDetailComponent } from './patient-timeline-detail.component';

describe('PatientTimelineDetailComponent', () => {
  let component: PatientTimelineDetailComponent;
  let fixture: ComponentFixture<PatientTimelineDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PatientTimelineDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PatientTimelineDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
