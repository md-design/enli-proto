import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ArtefactModel, ArtefactTypeEnum} from "../patient-timeline/artefact.model";
import {PlanService} from "../plan.service";
import {ProviderType, TaskModel} from "../models/task.model";
import {Observable} from "rxjs/Observable";
import {Store} from "@ngrx/store";
import {AppState} from "../models/app-state";
import {PeopleModel} from "../models/people.model";
import {TypeaheadMatch} from "@hxui/angular";

@Component({
  selector: 'app-patient-timeline-detail',
  templateUrl: './patient-timeline-detail.component.html',
  styleUrls: ['./patient-timeline-detail.component.scss'],
  styles:[':host{display:flex;flex-direction:column;flex:1;}']
})
export class PatientTimelineDetailComponent implements OnInit {

  @Output() onGoBack:EventEmitter<any> = new EventEmitter();

  tasks$: Observable<TaskModel[]>;
  linkMember: PeopleModel;
  ArtefactType = ArtefactTypeEnum;
  artefact:ArtefactModel;
  linkDocEditMode: boolean = false;
  showEditHint:boolean = false;
  people: PeopleModel[] = [
    new PeopleModel({id:1, name: 'Ms EatWell (Dietitian)', type: ProviderType.DIETITIAN }),
    new PeopleModel({id:2, name: 'John GP (Practitioner)', type: ProviderType.GP }),
    new PeopleModel({id:3, name: 'Sam Wellington (Specialist)', type: ProviderType.SPECIALIST }),
    new PeopleModel({id:4, name: 'Joe Blow (Nurse)', type: ProviderType.NURSE }),
    new PeopleModel({id:5, name: 'Lionel Messi (Practitioner)', type: ProviderType.GP })
  ];

  get hasLinkedMember(){
    return (this.artefact.linked);
  }

  constructor(private store: Store<AppState>,
              private service:PlanService) {
    this.tasks$ = this.store.select(state => state.tasks);
  }

  ngOnInit() {
      this.service.getArtefactDetail()
      .subscribe((artefact) => {
        this.artefact = artefact;
        if(!this.artefact.linked) {
          this.linkDocEditMode = true;
        }
      });
  }


  onLinkMember(event: TypeaheadMatch): void {
    this.artefact.linkedMember = event.item;
    this.artefact.linked = true;
    this.linkDocEditMode = false;
  }

  editLinkDoc(){
    if(!this.linkDocEditMode) {
      this.showEditHint = false;
      this.linkDocEditMode = true;
    }
  }

  goBack(){
    this.onGoBack.emit();
  }

  toggleEditHint(){
    this.showEditHint = (!this.linkDocEditMode)? !this.showEditHint : false;
  }

}
