import {Component, EventEmitter, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-summary',
  templateUrl: './summary.component.html',
  styleUrls: ['./summary.component.scss']
})
export class SummaryComponent implements OnInit {

  isEditMode:boolean = false;
  summaryText:string = "";
  showEditHint:boolean = false;
  constructor() { }

  ngOnInit() {
  }

  editSummary(){
    this.isEditMode = true;
  }

  cancel(){
    this.isEditMode = false;
    this.summaryText = "";
  }

  save(){
      this.isEditMode = false;
  }

  toggleEditHint(){
    this.showEditHint = (!this.isEditMode)? !this.showEditHint : false;
  }

}
