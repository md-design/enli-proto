import * as taskActions from './../actions/task.actions';
import {createSelector} from "@ngrx/store";

export function taskReducer(state = [], action: taskActions.Action){
  switch(action.type){
    case taskActions.LOAD_TASKS_SUCCESS: {
      return action.payload
    }
    case taskActions.ADD_TASK_SUCCESS: {
        return state.push(action.payload);
    }
    default : {
      return state
    }
  }
}
