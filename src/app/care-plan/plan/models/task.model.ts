export enum ProviderType {
  GP,
  DIETITIAN,
  SPECIALIST,
  NURSE,
  PATIENT,
  PODIATRIST,
  CARDIOLOGIST,
  PHYSIOTHERAPIST
}


export class TaskModel {
  id:number;
  title: string;
  description:string;
  dueDate:string;
  providers:ProviderType[] = [];
  complete: boolean = false;
  constructor(data?:TaskModel){
    Object.assign(this,data);
  }
}
