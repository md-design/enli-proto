import {ProviderType} from "./task.model";

export class PeopleModel {
  id:number;
  name: string;
  type:ProviderType;
  constructor(data?:PeopleModel){
    Object.assign(this,data);
  }
}
