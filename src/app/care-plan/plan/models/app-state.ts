import {TaskModel} from "./task.model";

export interface AppState {
  tasks: TaskModel[];
}
