import { Injectable } from '@angular/core';
import {ArtefactModel, ArtefactTypeEnum} from "./patient-timeline/artefact.model";
import {Observable} from "rxjs/Observable";
import {of} from "rxjs/observable/of";
import 'rxjs/add/operator/delay';
import {ProviderType, TaskModel} from "./models/task.model";

@Injectable()
export class PlanService {

  loadedArtefact:ArtefactModel;
  artefacts: ArtefactModel[] = [
    {
      date: 'Monday 13/07/2017',
      hcp:'Dr Greisel',
      description: 'Left knee',
      type: ArtefactTypeEnum.Radiology,
      linkedMember: null,
      linked : false
    },
    {
      date: 'Wednesday 12/06/2017',
      hcp:'Dr Greisel',
      description: 'Chest x-ray',
      type: ArtefactTypeEnum.Radiology,
      linkedMember: null,
      linked : false
    },
    {
      date: 'Monday 13/07/2017',
      hcp:'Dr Greisel',
      description: 'FBC',
      type: ArtefactTypeEnum.Pathology,
      linkedMember: null,
      linked : false
    },
    {
      date: 'Wednesday 12/06/2017',
      hcp:'Dr Greisel',
      description: 'Lipids',
      type: ArtefactTypeEnum.Pathology,
      linkedMember: null,
      linked : false
    },
    {
      date: 'Monday 13/07/2017',
      hcp:'',
      description: 'Specialist letter',
      type: ArtefactTypeEnum.CorrespondenceIn,
      linkedMember: null,
      linked : false
    },
    {
      date: 'Wednesday 12/06/2017',
      hcp:'',
      description: 'Residential Care Facility Correspendence',
      type: ArtefactTypeEnum.CorrespondenceIn,
      linkedMember: null,
      linked : false
    },
    {
      date: 'Tuesday 09/09/2016',
      hcp:'',
      description: 'Home medicine review',
      type: ArtefactTypeEnum.CorrespondenceOut,
      linkedMember: null,
      linked : false
    },
    {
      date: 'Monday 02/01/2015',
      hcp:'',
      description: 'Came in for a dr certificate',
      type: ArtefactTypeEnum.ConsultNotes,
      linkedMember: null,
      linked : false
    },
  ];
  tasks: TaskModel[] = [
    {
      id: 1,
      title: 'Hyperlipidemia',
      description: 'Refer to dietitian for advice and education, reduction in saturated fats.',
      dueDate: '2018-03-13T13:00:00.000Z',
      providers: [ProviderType.GP],
      complete: true
    },
    {
      id: 2,
      title: 'Obesity/Weight Loss',
      description: 'Advice on cholesterol lowering foods.',
      dueDate: '2018-03-13T13:00:00.000Z',
      providers: [ProviderType.DIETITIAN],
      complete: true
    }
    ];

  constructor() { }

  loadArtefact(index){
      this.loadedArtefact = new ArtefactModel(this.artefacts[index]);
  }

  getArtefactDetail(): Observable<ArtefactModel> {
    return of(this.loadedArtefact);
  }

  loadTasks():Observable<TaskModel[]> {
      return of(this.tasks);
  }

  addTask(task:TaskModel):Observable<TaskModel> {
      return of(task);
  }

}
