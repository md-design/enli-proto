import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {ArtefactModel, ArtefactTypeEnum} from "./artefact.model";
import {PlanService} from "../plan.service";

@Component({
  selector: 'app-patient-timeline',
  templateUrl: './patient-timeline.component.html',
  styleUrls: ['./patient-timeline.component.scss'],
  styles:[':host{display:flex;flex-direction:column;flex:1;}']
})
export class PatientTimelineComponent implements OnInit {

  @Output() onLoadArtefact:EventEmitter<number> = new EventEmitter<number>();

  ArtefactType = ArtefactTypeEnum;

  artefacts: ArtefactModel[] = [];

  filterArtefactsBy: ArtefactTypeEnum = ArtefactTypeEnum.All;

  constructor(private service:PlanService) { }

  ngOnInit() {
    this.artefacts = this.service.artefacts;
  }

  loadArtefact(index:number){
    this.onLoadArtefact.emit(index);
  }

}
