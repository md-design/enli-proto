import { Pipe, PipeTransform } from '@angular/core';
import {ArtefactModel, ArtefactTypeEnum} from "./artefact.model";

@Pipe({
  name: 'artefactType'
})
export class ArtefactTypePipe implements PipeTransform {

  transform(artefacts: ArtefactModel[], args?: any): any {
    if(args===ArtefactTypeEnum.All){
      return artefacts;
    }
    return artefacts.filter(artefact => artefact.type===args);
  }

}
