import {PeopleModel} from "../models/people.model";

export enum ArtefactTypeEnum {
  All,
  ConsultNotes,
  CorrespondenceIn,
  CorrespondenceOut,
  Pathology,
  Radiology
}

export class ArtefactModel{
  date: string;
  hcp: string;
  description: string;
  type: ArtefactTypeEnum;
  linkedMember: PeopleModel;
  linked:boolean;
  constructor(data?:ArtefactModel){
    Object.assign(this,data);
  }
}
