import {Component, ElementRef, OnInit, ViewChild, ViewChildren} from '@angular/core';
import {Goal} from "../../shared/components/goals/goal.model";
import {ResizeEvent} from "angular-resizable-element";
import {NgxCarouselStore} from "ngx-carousel";
import {PlanService} from "./plan.service";
import {Observable} from "rxjs/Observable";
import {Store} from "@ngrx/store";
import {AppState} from "./models/app-state";
import * as taskActions from './actions/task.actions';

@Component({
  selector: 'app-plan',
  templateUrl: './plan.component.html',
  styleUrls: ['./plan.component.scss'],
  styles: [':host{display:flex;flex-direction:column; flex:1;}']
})
export class PlanComponent implements OnInit {

  @ViewChild('timelineSidebar') timelineSidebar: ElementRef;


  showTimeline:boolean = true;
  minTimelineWidth:number = 357;
  tabs: any[] = [
    { title: 'Weight Loss', active: false },
    { title: 'Exercise', active: true },
    { title: 'Healthy Diet', active: false }
  ];

  goalsHealth: Goal = new Goal({title:'Goals and Health Needs', description: 'Aim for initial weight loss of 5%<br>Weight loss of 1kg per month'});
  goalsProgress: Goal = new Goal({title:'Progress', description: 'Weight loss has been difficult to achieve due to leg injury.'});


  constructor(private service:PlanService) {
  }

  ngOnInit() {
    this.timelineSidebar.nativeElement.style.width = this.minTimelineWidth+'px';
  }

  onResizeEnd(event: ResizeEvent): void {
    let newWidth = (event.rectangle.width < this.minTimelineWidth ) ? this.minTimelineWidth : event.rectangle.width;
    this.timelineSidebar.nativeElement.style.width = newWidth+"px";
  }

  addGoal(){
    this.tabs.push({
      title: 'Other',
      active: false
    });

    this.selectTab(this.tabs.length-1);
  }

  selectTab(i){
    this.tabs.forEach((step) => {
      step.active = false;
    });
    this.tabs[i].active = true;
  }

  onLoadArtefact(index: number) {
    this.showTimeline = false;
    this.service.loadArtefact(index);
  }




  deleteTasks(id: number){

  }


}
