import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import * as taskActions from "../actions/task.actions";
import {AppState} from "../models/app-state";
import {Store, select} from "@ngrx/store";
import {Observable} from "rxjs/Observable";
import {ProviderType, TaskModel} from "../models/task.model";
import {PlanService} from "../plan.service";
import {ModalService} from "@hxui/angular";
import {TaskModalComponent} from "./tasks-modal.component";
import {Router} from "@angular/router";

@Component({
  selector: 'app-tasks-correspondence',
  templateUrl: './tasks-correspondence.component.html',
  styleUrls: ['./tasks-correspondence.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TasksCorrespondenceComponent implements OnInit {

  tasks$: Observable<TaskModel[]>;

  constructor(private store: Store<AppState>,
              private planService: PlanService,
              private modalService: ModalService,
              private router: Router) {
    this.tasks$ = this.store.select(state => state.tasks);
  }

  ngOnInit() {
    this.getTasks();
  }

  getTasks(){
    this.store.dispatch(new taskActions.LoadTasksAction());
  }

  openModal = () => {
    this.modalService.create<TaskModalComponent>(TaskModalComponent, {
      onSuccess: (task) => {
        let provider = task.providers;
        task.providers = [Number.parseInt(provider)];
        console.log(task);
        this.store.dispatch(new taskActions.AddTaskAction(task));
        this.getTasks();
      }
    });
  }

  getProviderName(providers: ProviderType[]){
    switch(providers[0]){
      case ProviderType.GP :
        return "GP";
      case ProviderType.DIETITIAN:
        return "Dietitian";
      case ProviderType.NURSE:
        return "Nurse";
      case ProviderType.SPECIALIST:
        return "Specialist";
    }
  }

  gotoRoute(route:string){
    this.router.navigateByUrl(route);
  }

}
