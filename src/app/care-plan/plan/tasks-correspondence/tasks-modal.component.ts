import { Component, OnInit } from '@angular/core';
import {Modal} from '@hxui/angular';
import {ProviderType, TaskModel} from "../models/task.model";
import {IReactiveForm} from "../../../shared/interfaces/reactive-form.interface";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-custom-modal',
  styles:[
    ':host .hx-modal-card{overflow: initial;}',
    ':host .hx-modal-card-content{padding:1.5rem; overflow: initial;}',
    ':host .description{}'
  ],
  template: `
          <div class="hx-modal is-active">
            <div class="hx-modal-background"></div>
            <div class="hx-modal-card">
              <header class="hx-modal-card-head">
                <h1 class="hx-modal-card-title">Add Task</h1>
              </header>
              <section class="hx-modal-card-content">

                <form [formGroup]="form">
                  
                <div class="hx-columns">
                  <div class="hx-column">
                    <hxa-datepicker-form align="bottom" formControlName="dueDate"  [class.is-danger]="dueDate.invalid && dueDate.dirty"></hxa-datepicker-form>
                  </div>
                  <div class="hx-column">
                    <div class="hx-input-control" [class.is-danger]="providers.invalid && providers.dirty">
                      <div class="hx-select-control">
                        <select class="hx-select" id="providers" formControlName="providers">
                          <option value="">Please select a provider</option>
                          <option value="0">GP</option>
                          <option value="3">Nurse</option>
                          <option value="1">Dietitian</option>
                          <option value="2">Specialist</option>
                        </select>
                        <label for="provider" class="hx-label">Provider <sup>*</sup></label>
                      </div>
                    </div>
                  </div>
                </div>


                <div class="hx-columns">
                  <div class="hx-column">
                    <div class="hx-input-control"  [class.is-danger]="title.invalid && title.dirty">
                      <input class="hx-input" type="text" formControlName="title" required>
                      <label class="hx-label">Title <sup>*</sup></label>
                      <div class="hx-help">Please enter a task title</div>
                    </div>
                  </div>
                </div>

                <div class="hx-columns">
                  <div class="hx-column">
                    <div class="hx-input-control" [class.is-danger]="description.invalid && description.dirty">
                      <textarea name="textarea-sample" class="hx-textarea description" formControlName="description"  required></textarea>
                      <label for="textarea-sample" class="hx-label">Description</label>
                      <div class="hx-help">Please enter a task description</div>
                    </div>
                  </div>
                </div>

         
                </form>
              </section>
              <footer class="hx-modal-card-foot">
                <button class="hx-button" (click)="onCancel()">Cancel</button>
                <button class="hx-button is-primary" (click)="onSubmit()" [disabled]="!form.valid">Save</button>
              </footer>
            </div>
          </div>
        `
})

@Modal()
export class TaskModalComponent implements OnInit, IReactiveForm {

  form: FormGroup;
  submitted: boolean;
  taskFormModel: TaskModel = new TaskModel();
  isEdit: boolean = false;

  formErrors = {
    'title': '',
    'description':'',
    'dueDate':'',
    'providers':'',
    'complete':''
  };

  validationMessages = {
    'title': {
      'required':      'Task title is required'
    },
    'description': {
      'required':      'Task description is required'
    },
    'dueDate': {
      'required':      'Task due date is required'
    },
    'providers': {
      'required':      'Task requires at least 1 provider'
    }
  };

  get title() { return this.form.get('title'); }
  get description() { return this.form.get('description'); }
  get dueDate() { return this.form.get('dueDate'); }
  get providers() { return this.form.get('providers'); }


  constructor(private fb: FormBuilder){}

  ngOnInit() {
    this.buildForm();
  }

  protected onSuccess: Function;
  protected destroy: Function;
  protected close: Function;

  onCancel(): void{
    this.close();
    this.destroy();
  }


  buildForm(): void {
    this.form = this.fb.group({
      'title': [this.taskFormModel.title, Validators.required],
      'description': [this.taskFormModel.description, Validators.required],
      'dueDate': [this.taskFormModel.dueDate, Validators.required],
      'providers': [this.taskFormModel.providers, Validators.required],
      'complete': [this.taskFormModel.complete]
    });
    this.form.valueChanges
      .subscribe(data => this.onValueChanged(data));
    this.onValueChanged(); // (re)set validation messages now
  }

  onValueChanged(data?: any) {
    if (!this.form) { return; }
    const form = this.form;
    for (const field in this.formErrors) {
      // clear previous error message (if any)
      this.formErrors[field] = '';
      const control = form.get(field);
      if (control && control.dirty && !control.valid) {
        const messages = this.validationMessages[field];
        for (const key in control.errors) {
          this.formErrors[field] += messages[key] + ' ';
        }
      }
    }
  }

  onSubmit($event): void {
    if (this.form.valid) {
      this.submitted = true;
      this.taskFormModel = this.form.value;
      this.close();
      this.destroy();
      this.onSuccess(this.taskFormModel);
    }
  }
  }
