import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TasksCorrespondenceComponent } from './tasks-correspondence.component';

describe('TasksCorrespondenceComponent', () => {
  let component: TasksCorrespondenceComponent;
  let fixture: ComponentFixture<TasksCorrespondenceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TasksCorrespondenceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TasksCorrespondenceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
