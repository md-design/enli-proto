import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-metrics-measurements',
  templateUrl: './metrics-measurements.component.html',
  styleUrls: ['./metrics-measurements.component.scss']
})
export class MetricsMeasurementsComponent implements OnInit {

  metrics: any[] = [
    {
      measurement:{ label: 'Weight', value: '127', suffix: 'Kg', date: '02/01/2018', css: 'is-text-warning' },
      target: '< 100'
    },
    {
      measurement:{ label: 'BMI', value: '41.9', suffix: 'Kg/m2', date: '02/01/2018', css: 'is-text-warning' },
      target: '< 25'
    },
    {
      measurement:{ label: 'Waist circumference', value: '128', suffix: 'cm', date: '02/01/2018', css: 'is-text-warning' },
      target: '< 100'
    },
    {
      measurement:{ label: 'TSH', value: '2.0', suffix: 'mIU/L', date: '02/01/2018', css:'' },
      target: '0.4 - 4.0'
    }
  ];
  showHistory:boolean = false;


  constructor() { }

  ngOnInit() {
  }

  toggleHistory(){
    this.showHistory = !this.showHistory;
  }

}
