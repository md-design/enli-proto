import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MetricsMeasurementsComponent } from './metrics-measurements.component';

describe('MetricsMeasurementsComponent', () => {
  let component: MetricsMeasurementsComponent;
  let fixture: ComponentFixture<MetricsMeasurementsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MetricsMeasurementsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MetricsMeasurementsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
