import { Component, OnInit } from '@angular/core';
import {Event, NavigationEnd, NavigationError, Router} from "@angular/router";
import {Location} from "@angular/common";

@Component({
  selector: 'app-care-plan',
  templateUrl: './care-plan.component.html',
  styleUrls: ['./care-plan.component.scss'],
  styles: [':host{display:flex;flex-direction:column;flex:1;}']
})
export class CarePlanComponent implements OnInit {

  isSetup: boolean = true;
  isDashboard: boolean = false;
  currentStep = 0;

  stepperData = [
    {title:'Care Plan', showing: false, editable: true, complete: true, active: true, error:false, url:'/care-plan/plan'},
    {title:'Team Members', showing: false, editable: true, complete: false, active: false, error:true, url:'/care-plan/sharing/team-member/1'},
    {title:'Admin Tasks', showing: false, editable: true, complete: false, active: false, error:false, url:'/care-plan/sharing/admin-tasks'},
    {title:'Plan Documents', showing: false, editable: true, complete: false, active: false, error:false, url:'/care-plan/sharing/update-care-plan'},
  ];

  constructor( private location: Location,
               private router: Router)
  {
    router.events.subscribe((event: Event) => {
      if (event instanceof NavigationEnd) {
        this.isSetup = location.isCurrentPathEqualTo('/care-plan/setup');
        this.isDashboard = location.isCurrentPathEqualTo('/care-plan/dashboard');
        this.stepperData[0].showing = location.isCurrentPathEqualTo('/care-plan/plan');
        this.stepperData[2].showing = location.isCurrentPathEqualTo('/care-plan/sharing/admin-tasks');
        this.stepperData[3].showing = location.isCurrentPathEqualTo('/care-plan/sharing/update-care-plan');
        this.stepperData[1].showing = this.doesCurrentPathContain(location.path(), 'team-member');
      }

      if (event instanceof NavigationError) {
        // Hide loading indicator
        // Present error to user
        console.log(event.error);
      }
    });
  }

  ngOnInit() {
  }

  private doesCurrentPathContain(path,contains){
      let pathArr = path.split('/'),
          count = 0;
      for(let sector of pathArr){
        if(sector === contains)
          count++;
      }

      return (count>0);
  }

  gotoRoute(route:string){
    this.router.navigateByUrl(route);
  }

  private deactivateStep(){
    for(let step of this.stepperData){
      step.active = false;
    }
  }

  private activateStep(index){
    this.deactivateStep();
    if(!this.stepperData[index].error) {
      this.stepperData[index].active = true;
      this.stepperData[index].complete = true;
    }
  }

  next(){
    this.currentStep++;
    this.activateStep(this.currentStep);
    this.gotoRoute(this.stepperData[this.currentStep].url);
  }
}
