import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MedicationsComponent} from "./medications/medications.component";
import {ConditionsComponent} from "./conditions/conditions.component";
import {SetupComponent} from "./setup.component";
import {SharedModule} from "../../shared/shared.module";
import { SelectTemplateComponent } from './select-template/select-template.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule
  ],
  declarations: [
    SetupComponent,
    ConditionsComponent,
    MedicationsComponent,
    SelectTemplateComponent
  ]
})
export class SetupModule { }
