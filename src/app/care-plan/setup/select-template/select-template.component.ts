import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-select-template',
  templateUrl: './select-template.component.html',
  styleUrls: ['./select-template.component.scss'],
  styles:[':host{display:block;}']
})
export class SelectTemplateComponent implements OnInit {

  showList:boolean = true;
  patientProblems: Array<any> = [
    {
      checked: true,
      condition: 'Hyperlipedemia',
      edit: false
    },
    {
      checked: true,
      condition: 'Obesity',
      edit: false
    },
    {
      checked: true,
      condition: 'Hypertension',
      edit: false
    },
    {
      checked: false,
      condition: 'Osteoporosis',
      edit: false
    },
    {
      checked: false,
      condition: 'Asthma',
      edit: false
    },
    {
      checked: false,
      condition: 'Depression / Anxiety',
      edit: false
    }
  ];
  selectedTemplate:number = 0;
  templates: Array<any> = [
    { title : 'Multiple Chronic Conditions', description: 'Concise care plan for chronic conditions' },
    { title : 'Asthma', description: 'Detailed Asthma Plan' },
    { title : 'Chronic Heart Disease', description: 'Detailed Chronic Heart Disease Plan' },
    { title : 'Diabetes', description: 'Detailed Diabetes Plan' },
    { title : 'Hyperlipidemia', description: 'Detailed Hyperlipidemia Plan' },
    { title : 'Osteo-Arthritis', description: 'Detailed Osteo-Arthritis Plan' }
  ];

  constructor() { }

  ngOnInit() {
  }

  selectTemplate(i){
    this.selectedTemplate = i;
    this.showList = false;
  }

  goBack(){
    this.showList = true;
  }

  addNewPatientProblem(){
    this.patientProblems.push({
      checked: false,
      condition: '',
      edit: true
    });
  }

  savePatientProblem(i){
    this.patientProblems[i].edit = false;
  }

  cancelPatientProblem(i){
    this.patientProblems.splice(i, 1);
  }

}
