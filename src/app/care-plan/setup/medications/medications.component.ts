import {Component, EventEmitter, OnInit, Output, ViewChild} from '@angular/core';

@Component({
  selector: 'app-medications',
  templateUrl: './medications.component.html',
  styleUrls: ['./medications.component.scss']
})
export class MedicationsComponent implements OnInit {

  @Output() onSelectTab: EventEmitter<number> = new EventEmitter<number>();

  constructor() {

  }

  ngOnInit() {
  }


  selectTab(tab_id: number) {
    this.onSelectTab.emit(tab_id);
  }

}
