import {Component, EventEmitter, OnInit, Output, ViewChild} from '@angular/core';
import {
  ITabularConfig, TabsetComponent, TabularColumn, TabularColumnTypes,
  TabularSize
} from "@hxui/angular";

@Component({
  selector: 'app-conditions',
  templateUrl: './conditions.component.html',
  styleUrls: ['./conditions.component.scss']
})
export class ConditionsComponent implements OnInit {

  @Output() onSelectTab: EventEmitter<number> = new EventEmitter<number>();

  rowData: any[] = [];
  currentPage:number = 0;
  smallnumPages:number = 0;
  columnData: TabularColumn[] = [
    new TabularColumn('checkboxes', 'Checkboxes', TabularColumnTypes.Checkbox, false),
    new TabularColumn('condition', 'Condition', TabularColumnTypes.String, false, 'conditionsColumn'),
    new TabularColumn('date', 'Date', TabularColumnTypes.String, false, )
  ];

  tabularConfig: ITabularConfig = {
    size: TabularSize.Default,
    pagination: {
      itemsPerPage: 20,
      currentPage: 1
    },
  };

  constructor() {
    this.getTabularData();
  }

  ngOnInit() {
  }


  /**
   * Static data for example
   */
  private getTabularData() {
    this.rowData = [
      {
        id: 1,
        condition: 'LVF (Left Ventrictular Failure)',
        date: '2013',
        checked: true
      },
      {
        id: 2,
        condition: 'Ectropicon-Bileratal',
        date: '2013',
        checked: true
      },
      {
        id: 3,
        condition: 'Allergic Rhinitis',
        date: '2013',
        checked: true
      },
      {
        id: 4,
        condition: 'Frozen Shoulder',
        date: '2010',
        checked: true
      },
      {
        id: 5,
        condition: 'IHD (Ischaemic Heart Disease)',
        date: '2004',
        checked: true
      },
      {
        id: 6,
        condition: 'Obstructive Sleep Apnoea',
        date: '1995',
        checked: true
      },
      {
        id: 7,
        condition: 'Hypertension',
        date: '1995',
        checked: true
      },
      {
        id: 8,
        condition: 'Hyperlipedemia',
        date: '1995',
        checked: true
      },
      {
        id: 9,
        condition: 'Diabetes Mellitus - Type II',
        date: '1992',
        checked: true
      }
    ];
  }


  selectTab(tab_id: number) {
    this.onSelectTab.emit(tab_id);
  }


}
