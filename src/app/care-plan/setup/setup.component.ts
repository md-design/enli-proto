import {Component, OnInit, ViewChild} from '@angular/core';
import {
  ITabularConfig, TabsetComponent, TabularColumn, TabularColumnTypes,
  TabularSize
} from "@hxui/angular";
import {ITeamMembers, SharingService} from "../sharing/sharing.service";
import {TypeaheadMatch} from "@hxui/angular";

@Component({
  selector: 'app-setup',
  templateUrl: './setup.component.html',
  styles: [':host{display:flex;flex:1;}']
})
export class SetupComponent implements OnInit {
  @ViewChild('planSummaryTabs') planSummaryTabs: TabsetComponent;

  patientConsent: boolean = false;
  currentStep;
  people: ITeamMembers[] = [];
  linkMember: string = '';

  stepperData = [
    {title:'Patient Consent', editable: true, complete: false, active: true},
    {title:'Select Primary GP', editable: false, complete: false, active: false},
    {title:"Horatio (Bud)'s Goal", editable: false, complete: false, active: false},
    {title:'Health Summary', editable: false, complete: false, active: false},
    {title:'Plan Details', editable: false, complete: false, active: false}
  ];

  constructor(private service: SharingService) {
    this.service.getHcps().subscribe((people)=>{
      this.people = people;
    });
  }

  ngOnInit() {
  }

  get showResults():boolean {
    return (this.linkMember.length > 0);
  };

  onLinkMember(event: TypeaheadMatch): void {
    // this.artefact.linkedMember = event.item;;
  }

  getProviderTypeName(type){
    return this.service.getProviderTypeName(type);
  }

  hasAssigned(member:ITeamMembers){
    return this.service.hasAssigned(member);
  }


  selectTab(tab_id: number) {
    this.planSummaryTabs.tabs[tab_id].active = true;
  }

  selectStep(step: number) {

    if(this.currentStep){
      this.currentStep.complete = true;
      this.currentStep.editable = true;
    }
      this.stepperData.forEach((step) => {
        step.active = false;
      });

      this.stepperData[step].active = true;
      this.currentStep = this.stepperData[step];
  }

  gotoPlanDetails(){
    this.stepperData[4].editable = true;
    this.stepperData[3].complete = true;
    this.selectStep(4);
  }

  onPatientConsentChange(eve: any) {
    this.patientConsent = !this.patientConsent;
    this.stepperData[1].editable = this.patientConsent;
    this.stepperData[0].complete = this.patientConsent;
  }

}
