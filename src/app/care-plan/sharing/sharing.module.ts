import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharingComponent } from './sharing.component';
import {SharedModule} from "../../shared/shared.module";
import { UpdateCarePlanComponent } from './update-care-plan/update-care-plan.component';
import { TeamMemberComponent } from './team-member/team-member.component';
import {EditContactModalComponent} from "./team-member/edit-contact.modal";
import {SharingService} from './sharing.service';
import {AssignProviderModalComponent} from "./team-member/assign-provider.modal";
import { AdminTasksComponent } from './admin-tasks/admin-tasks.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule
  ],
  declarations: [
    SharingComponent,
    UpdateCarePlanComponent,
    TeamMemberComponent,
    EditContactModalComponent,
    AssignProviderModalComponent,
    AdminTasksComponent
  ],
  entryComponents:[
    EditContactModalComponent,
    AssignProviderModalComponent
  ],
  providers:[
    SharingService
  ]
})
export class SharingModule { }
