import { Component, OnInit } from '@angular/core';
import {Modal} from '@hxui/angular';
import {IReactiveForm} from "../../../shared/interfaces/reactive-form.interface";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Observable} from "rxjs/Observable";
import {ITeamMembers, SharingService} from "../sharing.service";
import {PeopleModel} from "../../plan/models/people.model";
import {TypeaheadMatch} from "@hxui/angular";

@Component({
  selector: 'app-assign-provider-modal',
  styles:[
    ':host {}',
    ':host .hx-modal-card{overflow: initial;}',
    ':host .hx-modal-card-content{padding:1.5rem; overflow: initial;position:relative;}',
    ':host .description{}'
  ],
  template: `
          <div class="hx-modal is-active">
            <div class="hx-modal-background"></div>
            <div class="hx-modal-card">
              <header class="hx-modal-card-head">
                <h1 class="hx-modal-card-title">Assign Provider</h1>
                <button class="delete" (click)="onCancel()"></button>
              </header>
              <section class="hx-modal-card-content">

                <div class="hx-input-group">
                  <div class="hx-input-control">
                    <input class="hx-input" type="text"   
                           [(ngModel)]="linkMember"
                           required
                           appAutoFocus>
                    <label class="hx-label is-text-uppercase"><i class="icon icon-search is-small"></i> Search for team member</label>
                    <div class="hx-help">Search for a team member for this role by name or by speciality</div>
                  </div>
                    <i class="hx-icon icon-close-empty clearBtn" *ngIf="showResults" (click)="linkMember=''"></i>
                </div>
                

                <ng-container *ngIf="showResults">
                  <div class="hx-flex hx-flex-justify-between hx-flex-align-center mt-9 my-4">
                  <h6 class="is-text-uppercase my-0">Results</h6>
                </div>
                  <table class="hx-table is-striped results">
                    <thead>
                    <tr>
                      <th class="results__name">Name</th>
                      <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr *ngFor="let person of people">
                      <td *ngIf="hasAssigned(person)"><b>{{person.assigned.name}}</b><br>{{getProviderTypeName(person.type)}}</td>
                      <td *ngIf="!hasAssigned(person)"><b>Joe Blow</b><br>{{getProviderTypeName(person.type)}}</td>
                      <td><div class="hx-flex"><i class="hx-icon icon-information is-info mr-1" [hxTooltip]="dynamicContentTooltip" ></i><button class="hx-button is-small" (click)="onAssign(person)">Assign</button></div></td>
                    </tr>
                    </tbody>
                  </table>
                </ng-container>

                <hx-tooltip-content #dynamicContentTooltip [animation]="true" placement="left">
                  <div class="has-text-left">
                    <b>Eating Well Dietitians</b><br>
                    123 Any Street<br>
                    Wellington, NSW 2334
                
                    <i class="hx-icon icon-email-outline"></i>telly.eatwell@eatingwell.com.au<br>
                    <i class="hx-icon icon-iphone"></i> 0405768234<br>
                    <i class="hx-icon icon-telephone-outline"></i> 02 9123 3456
                  </div>
                </hx-tooltip-content>.

              <ng-container *ngIf="!showResults">
                <div class="hx-flex hx-flex-justify-between hx-flex-align-center mt-9 my-4">
                  <h6 class="is-text-uppercase my-0">Recent</h6>
                </div>
                <table class="hx-table is-striped">
                  <thead>
                  <tr>
                    <th>Name</th>
                    <th><a href="#"><i class="hx-icon icon-arrow-up"></i>Last referred</a></th>
                    <th class="has-text-centered">Referrals</th>
                    <th></th>
                  </tr>
                  </thead>
                  <tbody>
                  <tr>
                    <td><b>Ms Rowena Runwell</b><br>Physiotherapist</td>
                    <td>12/10/2017</td>
                    <td class="has-text-centered">7</td>
                    <td><button class="hx-button is-small">Assign</button></td>
                  </tr>
                  <tr>
                    <td><b>Mr Trevor Stretchwell</b><br>Physiotherapist</td>
                    <td>22/09/2017</td>
                    <td class="has-text-centered">5</td>
                    <td><button class="hx-button is-small">Assign</button></td>
                  </tr>
                  <tr>
                    <td><b>Mr Barry Bolt</b><br>Physiotherapist</td>
                    <td>06/03/2017</td>
                    <td class="has-text-centered">2</td>
                    <td><button class="hx-button is-small">Assign</button></td>
                  </tr>

                  </tbody>
                </table>
              </ng-container>

              </section>
            </div>
          </div>
        `
})

@Modal()
export class AssignProviderModalComponent implements OnInit{

  people: ITeamMembers[] = [];
  linkMember: string = '';

  protected onSuccess: Function;
  protected close: Function;

  constructor(private service: SharingService) {
    this.service.getTeamMembers().subscribe((people)=>{
      this.people = people;
    });
  }

  ngOnInit() {
  }

  get showResults():boolean {
    return (this.linkMember.length > 0);
  };

  onLinkMember(event: TypeaheadMatch): void {
   // this.artefact.linkedMember = event.item;;
  }

  getProviderTypeName(type){
    return this.service.getProviderTypeName(type);
  }

  hasAssigned(member:ITeamMembers){
    return this.service.hasAssigned(member);
  }


  onCancel = () => {
    this.close();
  }

  onOk = () => {
    this.onSuccess('Callback');
    this.close();
  }

  onAssign = (person) =>{
    this.onSuccess(person);
    this.close();
  }
}
