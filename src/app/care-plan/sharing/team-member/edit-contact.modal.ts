import { Component, OnInit } from '@angular/core';
import {Modal} from '@hxui/angular';
import {IReactiveForm} from "../../../shared/interfaces/reactive-form.interface";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-edit-contact-modal',
  styles:[
    ':host .hx-modal-card{overflow: initial;}',
    ':host .hx-modal-card-content{padding:1.5rem; overflow: initial;}',
    ':host .description{}'
  ],
  template: `
          <div class="hx-modal is-active">
            <div class="hx-modal-background"></div>
            <div class="hx-modal-card">
              <header class="hx-modal-card-head">
                <h1 class="hx-modal-card-title">Edit Contact Details</h1>
                <button class="delete" (click)="onCancel()"></button>
              </header>
              <section class="hx-modal-card-content">
                <p>Team members contact details can only be changed from your clincal system’s local address book.</p>

                 <p>After making desired changes to a team member’s details, the changes will be reflected automatically on the care plan.</p>
              </section>
              <footer class="hx-modal-card-foot">
                <button class="hx-button is-primary" (click)="onOk()">Ok</button>
              </footer>
            </div>
          </div>
        `
})

@Modal()
export class EditContactModalComponent implements OnInit{

  protected onSuccess: Function;
  protected close: Function;

  constructor() {}

    ngOnInit() {
    }

    onCancel = () => {
      this.close();
    }

    onOk = () => {
      this.close();
    }
}
