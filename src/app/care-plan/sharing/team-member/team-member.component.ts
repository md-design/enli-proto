import { Component, OnInit } from '@angular/core';
import {ModalService} from "@hxui/angular";
import {EditContactModalComponent} from "./edit-contact.modal";
import {ActivatedRoute} from "@angular/router";
import {ITeamMembers, SharingService} from "../sharing.service";
import {AssignProviderModalComponent} from "./assign-provider.modal";

@Component({
  selector: 'app-team-member',
  templateUrl: './team-member.component.html',
  styleUrls: ['./team-member.component.scss'],
  styles:[':host{display:flex;flex-direction:column;}']
})
export class TeamMemberComponent implements OnInit {

  teamProviderId:number;
  teamMember:ITeamMembers;

  constructor(private modalService: ModalService,
              private route: ActivatedRoute,
              private sharingService: SharingService) { }

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      this.teamProviderId = parseInt(params.get('id'));
      this.teamMember = this.sharingService.getTeamMemberById(this.teamProviderId);
    });
  }

  getProviderTypeName(type){
    return this.sharingService.getProviderTypeName(type);
  }

  hasAssigned(member:ITeamMembers){
    return this.sharingService.hasAssigned(member);
  }

  openModal = () => {
    this.modalService.create<EditContactModalComponent>(EditContactModalComponent, {
      onSuccess: (data) => {
        alert(data);
      }
    });
  }

  assignProviderModal = () => {
    this.modalService.create<AssignProviderModalComponent>(AssignProviderModalComponent, {
      onSuccess: (person) => {
        this.teamMember = person;
        this.teamProviderId = person.id;
      }
    });
  }



}
