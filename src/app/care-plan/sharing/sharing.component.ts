import { Component, OnInit } from '@angular/core';
import {ITeamMembers, SharingService} from "./sharing.service";
import {Observable} from "rxjs/Observable";
import {Event, NavigationEnd, NavigationError, Router} from "@angular/router";
import {Location} from "@angular/common";

@Component({
  selector: 'app-sharing',
  templateUrl: './sharing.component.html',
  styleUrls: ['./sharing.component.scss'],
  styles: [':host{display:flex;flex-direction:column; flex:1;}']
})
export class SharingComponent implements OnInit {

  teamMembers$: Observable<ITeamMembers[]>;
  isGeneratePlan:boolean = false;
  constructor(private service: SharingService,
              private location: Location,
              private router: Router) {
    this.teamMembers$ = this.service.getTeamMembers();
    router.events.subscribe((event: Event) => {
      if (event instanceof NavigationEnd) {
        this.isGeneratePlan = location.isCurrentPathEqualTo('/care-plan/sharing/update-care-plan');
      }

      if (event instanceof NavigationError) {
        // Hide loading indicator
        // Present error to user
        console.log(event.error);
      }
    });
  }

  ngOnInit() {
  }

  hasAssigned(member:ITeamMembers){
    return this.service.hasAssigned(member);
  }

  getProviderTypeName(type){
   return this.service.getProviderTypeName(type);
  }

}
