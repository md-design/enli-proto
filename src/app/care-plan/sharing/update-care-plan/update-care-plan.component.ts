import { Component, OnInit } from '@angular/core';
import 'rxjs/operator/delay';

@Component({
  selector: 'app-update-care-plan',
  templateUrl: './update-care-plan.component.html',
  styleUrls: ['./update-care-plan.component.scss']
})
export class UpdateCarePlanComponent implements OnInit {

  isPublished = false;
  isPublishing = false;

  constructor() { }

  ngOnInit() {
  }

  onPublishHandler(){
    this.isPublishing = true;
   setTimeout(()=>{
     this.isPublished = true;
     this.isPublishing = false;
   },5000);
  }

}
