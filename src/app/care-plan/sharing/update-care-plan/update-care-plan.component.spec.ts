import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateCarePlanComponent } from './update-care-plan.component';

describe('UpdateCarePlanComponent', () => {
  let component: UpdateCarePlanComponent;
  let fixture: ComponentFixture<UpdateCarePlanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateCarePlanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateCarePlanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
