import { Injectable } from '@angular/core';
import {ProviderType} from "../plan/models/task.model";
import {Observable} from "rxjs/Observable";
import {of} from "rxjs/observable/of";


export interface ITeamMembers{
  id:number;
  type:ProviderType;
  assigned:{name?};
}

@Injectable()
export class SharingService {

  teamMembers:ITeamMembers[] = [
    // {
    //   // id: 1,
    //   // type: ProviderType.PATIENT,
    //   // assigned: {
    //   //   name: 'Horatio Farnswell',
    //   // }
    // },
    {
      id:2,
      type: ProviderType.DIETITIAN,
      assigned: {
        name: 'Tilly Eatwell',
      }
    },
    {
      id:3,
      type: ProviderType.PODIATRIST,
      assigned:{
        name: 'Dr Dave Footwell'
      }
    },
    {
      id:4,
      type: ProviderType.CARDIOLOGIST,
      assigned:{
        name: 'Dr Eva Beatwell'
      }
    },
    {
      id:5,
      type: ProviderType.PHYSIOTHERAPIST,
      assigned:{}
    },
    // {
    //   id: 7,
    //   type: ProviderType.GP,
    //   assigned: {
    //     name: 'Dr Fiona Getwell',
    //   }
    // }
  ];

  hcps:ITeamMembers[] = [
    {
      id: 1,
      type: ProviderType.GP,
      assigned: {
        name: 'Dr John GP',
      }
    },
    {
      id: 2,
      type: ProviderType.GP,
      assigned: {
        name: 'Dr Fiona Getwell',
      }
    },
    {
      id: 3,
      type: ProviderType.GP,
      assigned: {
        name: 'Dr Bob FeelBetter',
      }
    }
  ];

  constructor() { }


  getTeamMembers(): Observable<ITeamMembers[]> {
    return of(this.teamMembers);
  }

  getTeamMemberById(id){
    return this.teamMembers.find((member:ITeamMembers) => {
      return member.id === id;
    });
  }

  setTeamMemberById(id, name){
    let member = this.teamMembers.find((member:ITeamMembers) => {
      return member.id === id;
    });
    if(member){
      member.assigned.name = name;
    }
  }

  getHcps(): Observable<ITeamMembers[]> {
    return of(this.hcps);
  }

  getProviderTypeName(type){
    switch(type){
      case ProviderType.GP :
        return "GP";
      case ProviderType.DIETITIAN:
        return "Dietitian";
      case ProviderType.NURSE:
        return "Nurse";
      case ProviderType.SPECIALIST:
        return "Specialist";
      case ProviderType.PATIENT:
        return "Patient";
      case ProviderType.CARDIOLOGIST:
        return "Cardiologist";
      case ProviderType.PHYSIOTHERAPIST:
        return "PHYSIOTHERAPIST";
      case ProviderType.PODIATRIST:
        return "PODIATRIST";
    }
  }

  hasAssigned(member:ITeamMembers){
    return  (typeof member.assigned.name !== 'undefined');
  }
}
