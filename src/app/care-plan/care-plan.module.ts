import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CarePlanComponent } from './care-plan.component';
import {SharedModule} from "../shared/shared.module";
import {PlanModule} from "./plan/plan.module";
import {SharingModule} from "./sharing/sharing.module";

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    PlanModule,
    SharingModule
  ],
  declarations: [CarePlanComponent]
})
export class CarePlanModule { }
