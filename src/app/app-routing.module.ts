import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {PageNotFoundComponent} from './page-not-found/page-not-found.component';
import {SetupComponent} from "./care-plan/setup/setup.component";
import {CarePlanComponent} from "./care-plan/care-plan.component";
import {PlanComponent} from "./care-plan/plan/plan.component";
import {SharingComponent} from "./care-plan/sharing/sharing.component";
import {UpdateCarePlanComponent} from "./care-plan/sharing/update-care-plan/update-care-plan.component";
import {TeamMemberComponent} from "./care-plan/sharing/team-member/team-member.component";
import {DashboardComponent} from "./care-plan/dashboard/dashboard.component";
import {AdminTasksComponent} from "./care-plan/sharing/admin-tasks/admin-tasks.component";
import {TrackingComponent} from "./tracking/tracking.component";


const routes: Routes = [
  { path: '', redirectTo: '/care-plan/setup', pathMatch: 'full' },
  { path: 'tracking', component: TrackingComponent },
  {
    path: 'care-plan',
    component: CarePlanComponent,
    children: [
      { path: '', redirectTo: '/care-plan/plan', pathMatch: 'full' },
      { path: 'dashboard', component: DashboardComponent },
      { path: 'setup',  component: SetupComponent },
      { path: 'plan',  component: PlanComponent },
      {
        path: 'sharing',
        component: SharingComponent ,
        children: [
          {
            path: '',
            redirectTo: '/care-plan/sharing/update-care-plan',
            pathMatch: 'full'
          },
          {
            path: 'update-care-plan',
            component: UpdateCarePlanComponent
          },
          {
            path: 'team-member/:id',
            component: TeamMemberComponent
          },
          {
            path: 'admin-tasks',
            component: AdminTasksComponent
          }
        ]}
    ]
  },
  { path: '**',  component: PageNotFoundComponent },
];
@NgModule({
  imports: [ RouterModule.forRoot(routes, { useHash: true }) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
