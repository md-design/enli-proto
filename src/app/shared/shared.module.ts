import {ModuleWithProviders, NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';
import {BrowserModule} from '@angular/platform-browser';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AppRoutingModule} from '../app-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {HxUiModule} from '@hxui/angular';
import { ClickOutsideDirective } from './directives/click-outside.directive';
import { StickyHeaderDirective } from './directives/sticky-header.directive';
import {HttpClientModule} from "@angular/common/http";
import { ResizableModule } from 'angular-resizable-element';
import {NgxCarouselModule} from "ngx-carousel";
import { AutoFocusDirective } from './directives/auto-focus.directive';
import {GoalsComponent} from "./components/goals/goals.component";


@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    CommonModule,
    AppRoutingModule,
    HxUiModule,
    BrowserAnimationsModule,
    ResizableModule,
    NgxCarouselModule
  ],
  declarations: [
    ClickOutsideDirective,
    StickyHeaderDirective,
    AutoFocusDirective,
    GoalsComponent
  ],
  exports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    CommonModule,
    AppRoutingModule,
    HxUiModule,
    BrowserAnimationsModule,
    ClickOutsideDirective,
    StickyHeaderDirective,
    ResizableModule,
    NgxCarouselModule,
    AutoFocusDirective,
    GoalsComponent
  ]
})
export class SharedModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: SharedModule,
      providers: [
      ]
    };
  }
}
