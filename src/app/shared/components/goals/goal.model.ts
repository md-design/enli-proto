
export class Goal {

  title: string;
  description: string;

  constructor(data?:Goal){
    Object.assign(this,data);
  }
}
