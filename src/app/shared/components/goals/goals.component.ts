import {Component, Input, OnInit} from '@angular/core';
import {Goal} from "./goal.model";

@Component({
  selector: 'app-goals',
  templateUrl: './goals.component.html',
  styleUrls: ['./goals.component.scss'],
  styles: [':host{display:block;}']
})
export class GoalsComponent implements OnInit {

 @Input() goal: Goal;
  @Input() showRemoveGoal:boolean = false;

 showEditHint:boolean = false;
 isEditMode:boolean = false;


  constructor() { }

  ngOnInit() {
  }

  editMode(){
    if(!this.isEditMode) {
      this.showEditHint = false;
      this.isEditMode = true;
    }
  }

  cancel($event){
    $event.stopPropagation();
    this.isEditMode = false;
  }

  save($event){
    $event.stopPropagation();
    this.isEditMode = false;
  }

  toggleEditHint(){
    this.showEditHint = (!this.isEditMode)? !this.showEditHint : false;
  }

}
