import {Directive, ElementRef, HostListener, Input, Renderer2} from '@angular/core';

@Directive({
  selector: '[hx-sticky]'
})
export class StickyHeaderDirective {

  private scrollablePosition: number = 0;

  @Input() addClass: string = 'fixed';
  @Input() offSet: number = 20;

  constructor(private el: ElementRef, private render:Renderer2) {

  }

  private addSticky() {
    this.el.nativeElement.style.position = 'fixed';
    this.el.nativeElement.style.top = this.offSet + 'px';
    this.render.addClass(this.el.nativeElement, this.addClass);
  }

  private removeSticky() {
    this.el.nativeElement.style.position = '';
    this.render.removeClass(this.el.nativeElement, this.addClass);
  }

  @HostListener("scroll", [])
  onWindowScroll() {

    let offset: number = this.el.nativeElement.offsetTop;
    this.scrollablePosition = this.el.nativeElement.scrollTop || 0;

    console.log(offset,this.scrollablePosition);

    if (this.scrollablePosition > this.offSet) {

     this.addSticky();
    } else {
      this.removeSticky();
    }
  }

}
