import {FormGroup} from "@angular/forms";

/**
 * This interface is to ensure all the correct form stuff is implemented.
 * @type ReactiveFormModule
 */
export interface IReactiveForm {
  /**
   * ngOnInit is called right after the directive's data-bound properties have been
   * checked for the first time, and before and of its children have been checked.
   * It is invoked only once when the directive is instantiated.
   */
  ngOnInit(): void;

  /**
   * Used to group form controls and validators as 1 smart form.
   */
  form: FormGroup;

  /**
   * Used to determine if the form has been submitted.
   * Usually used to hide/disable submit button on click.
   */
  submitted: boolean;

  /**
   * Used to determine if form is in edit mode.
   * could be a getter if you need something more complicated.
   */
  isEdit: boolean;

  /**
   * Hold errors as object\s
   *
   * @example
   * formErrors = {
   *   'name': '',
   *   'power': ''
   * };
   */
  formErrors: any;


  /**
   * Hold error messages as key=>val pairs
   *
   * @example
   * validationMessages = {
   * 'name': {
   *  'required':      'Name is required.',
   *  'minlength':     'Name must be at least 4 characters long.',
   *  'maxlength':     'Name cannot be more than 24 characters long.',
   *  'forbiddenName': 'Someone named "Bob" cannot be a hero.'
   * },
   * 'power': {
   *  'required': 'Power is required.'
   * }
   * };
   */
  validationMessages: any;

  /**
   * Add validation config and handlers.
   * Should be called on ngOnInit
   */
  buildForm(): void;


  /**
   * From here we should do all the error checking.
   * @param data
   */
  onValueChanged(data?: any);


  /**
   * Form submit handler
   * @param {$event}
   */
  onSubmit($event): void;
}
